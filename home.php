<?php

if(isset($_POST['number'])){
  $number = $_POST['number'];
}

if(isset($_POST['capitalizar'])){
  if($_POST['capitalizar'] == 'on'){
    $capitalizar = $_POST['capitalizar'];
  }

}

if(isset($_POST['especial'])){
  if($_POST['especial'] == 'on'){
    $especial = true;
  }

}

include './static/php/cookies.php';

include './static/php/functions.php';




$capitalizar = false;
$especial = false;
$number = 8;
if(isset($_POST['number'])){
  $number = $_POST['number'];
}

if(isset($_POST['capitalizar'])){
  if($_POST['capitalizar'] == 'on'){
    $capitalizar = $_POST['capitalizar'];
  }

}

if(isset($_POST['especial'])){
  if($_POST['especial'] == 'on'){
    $especial = true;
  }

}

contrasenya_random($number,$capitalizar,$especial);



function contrasenya_random($len = 8, $catitalize= false, $especial_char = false){
    global $password;
    
    // Restamos dos carateres para el número final.
    $length = $len - 2;
    // Revisamos que los parametros que recibimos cumplan con lo que necesitamos
    if($len >=6 && ($length %2)!= 0){
        $len = 8;
    }

    $conso = array('b','c','d','f','g','h','j','k','l','m','n','p','r','s','t','v','w','x','y','z');
    
    $vocal = array('a','e','i','o','u');
    
    $spchars = array('!','@','#','$','%','^','*','&','*','-','+','?');
    
    $password = '';

    for($i=1;$i<=($length/2);$i++){
      $password .=$conso[rand(0,19)];
      $password .= $vocal[rand(0,4)];
    }

    if ($catitalize == true){
      $password = ucfirst($password);
    }
    if($especial_char == true){
      $password = substr($password,0,-1).$spchars[rand(0,11)];
    }
    $password .= rand(10,99);
    return $password;
}



include $data;

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

  <title>E02 Sintaxi de variables</title>
</head>

<body>
  <nav class="nav nav-fill">
    <li class="nav-item">
      <a class="nav-link active" href="#">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="./home.php"><?php echo $ejercicio?> 1</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="./ejercici2.php"><?php echo $ejercicio?> 2</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="./ejercici3.php"><?php echo $ejercicio?> 3</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="./ejercici4.php"><?php echo $ejercicio?> 4</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="ejercici5.php"><?php echo $ejercicio?> 5</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="http://el-informatic-antisistema.cat/" target="_blank">web personal</a>
    </li>
    <div class=" col-11 text-center m-2">
      <a href="http://localhost:8080/php/home.php?lang=es"><img
          src="https://www.flaticon.es/svg/static/icons/svg/197/197593.svg" heigth="50px" , width="50px"></a>
      <a href="http://localhost:8080/php/home.php?lang=ca"><img
          src="https://www.flaticon.es/svg/static/icons/svg/553/553376.svg" heigth="50px" , width="50px"></a>
      <a href="http://localhost:8080/php/home.php?lang=de"><img
          src="https://www.flaticon.es/svg/static/icons/svg/197/197571.svg" heigth="50px" , width="50px"></a>
      <a href="http://localhost:8080/php/home.php?lang=fr"><img
          src="https://www.flaticon.es/svg/static/icons/svg/197/197560.svg" heigth="50px" , width="50px"></a>
      <a href="http://localhost:8080/php/home.php?lang=en"><img
          src="https://www.flaticon.es/svg/static/icons/svg/197/197374.svg" heigth="50px" , width="50px"></a>
      <a href="http://localhost:8080/php/home.php?lang=eu"><img
          src="https://www.flaticon.es/svg/static/icons/svg/197/197517.svg" heigth="50px" , width="50px"></a>
    </div>
  </nav>

  <div class="container">

    <img class="img-fluid" src="./static/img/img1.jpg" alt="">
    <br />
    <h1><strong><?php echo $ejercicio_1_h1 ?></strong></h1>
    <div class="row">
      <br />
      <form action="home.php" method="post">
        <div class="form-group">
          <label for="exampleInputEmail1">Selecciona tu número</label>
          <input list="numbers" class="form-control" id="number" name="number" required>
          <datalist id="numbers">
            <option value="8">
            <option value="10">
            <option value="12">
            <option value="14">
            <option value="16">
            <option value="18">
            <option value="20">
          </datalist>
        </div>

        <div class="form-group form-check">
          <input type="checkbox" class="form-check-input" id="capitalizar" name="capitalizar">
          <label class="form-check-label" for="capitalizar">Capitalizar?</label>
        </div>
        <div class="form-group form-check">
          <input type="checkbox" class="form-check-input" id="especial" name="especial">
          <label class="form-check-label" for="especial">Valor especial</label>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
      </form>
      <div class="jumbotron jumbotron-fluid ml-5">
        <div class="container">
          <h1 class="display-4"><?php echo $password?></h1>
          <p class="lead">Las contraseñas han de ser seguras ;)</p>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-5">
          <img src="./static/img/bolsa.jpg" alt="imagen de la bolsa" heigth="300px" width="300px">
        </div>
        <div class="col-5">
          <p>La bolsa de Tony</p>
          <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id, qui. Debitis sunt quas repellendus placeat
            rem nemo commodi necessitatibus in.</p>
        </div>
      </div>


    </div>
  </div>
  </div>


  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
  </script>


</body>

</html>