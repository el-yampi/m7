<?php
session_start();

// E06 - Web scraping

// Web scraping (de l'anglès to scrap 'rasclar') és una tècnica de programari o software informàtic per extreure informació dels llocs web. En general, aquest tipus de programes de software simulen l'exploració humana del World Wide Web, ja sigui amb la implementació de baix nivell de protocol de transferència d'hipertext (HTTP), o amb la incorporació d'un navegador web.

// El web scraping està molt relacionat amb la indexació de la web, que indexa informació de la web utilitzant un robot. Aquesta tècnica és una tècnica universal adoptada per la majoria dels motors de cerca. Per contra, el web scraping es centra més en la transformació de les dades no estructurades al web, generalment en format HTML, en dades estructurades que poden ser emmagatzemades i analitzades en una base de dades local, central o de full de càlcul. El web scraping també està relacionat amb l'automatització del web, que simula la navegació humana utilitzant software d'ordinador. Algun dels usos principals del web scraping són la comparació de preus en botigues, monitorar dades relacionades amb el clima de certa regió, detectar canvis en llocs webs o la integració de dades en llocs web.

// En aquesta pràctica simularem aquesta tècnica i capturarem cotitzacions d'accions del mercat continu de valors.Per això farem servir la pàgina web d'Inversis (https://www.inversis.com/inversiones/productos/cotizaciones-nacionales&pathMenu=3_1_0_0&esLH=N)

// Ens interesa generar un array associatiu de dos dimensions:

// cotitzacions { "ANA" => { "nom" => "ACCIONA",
//                           "data" = 2020-09-01,
//                           "hora" = 10:12,
//                           "cot" = 94,20000 } , 
//                "ACX" => { "nom" => "ACERINOX",
// [...]
include_once './static/php/functions.php';
$ruta = "https://www.inversis.com/inversiones/productos/cotizaciones-nacionales&pathMenu=3_1_0_0&esLH=N";
$files_pages = file_get_contents($ruta);
$datos_finales = gen_array_coti($files_pages);

if(!isset($_SESSION['old_cotis'])){
    $_SESSION["old_cotis"] = $datos_finales;
}






?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla Tony m7</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
        <h1> La Tabla de cotización de Tony:</h1>

        <?php
            echo gen_cabecera($datos_finales);
            echo gen_tbody($datos_finales);

        ?>
        <form action="ejercici5.php" method="post">
        <input id="actualizar" name="actualizar" type="hidden" value="borrar_array">
        <button type="submit" class="btn btn-danger m-5 text-center">Actualizar</button>
        </form>
        </div>

    </div>





    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>