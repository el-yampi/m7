<?php
session_start();
include './static/php/functions.php';
include './static/php/cookies.php';
include $data;
include './static/includes/header.php';
include './static/includes/footer.php';
include './static/php/functions/formu_tony.php';
include './static/php/functions/xml.php';

$nombre=null;
$email=null;
$mensaje=null;
$post=false;

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
  $post = true;
  if(isset($_POST['nombre'])){
    $nombre = $_POST['nombre'];
  }
  if(isset($_POST['email'])){
    $email = $_POST['email'];
  }
  if(isset($_POST['mensaje'])){
    $mensaje = $_POST['mensaje'];
  }
}

$formu_tony = gen_formu($nombre,$email,$mensaje,$post);



?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web de Tony m7</title>

  <!-- Bootstrap core CSS -->
  <link href="./static/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="./static/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='./static/https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='./static/https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="./static/css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  

  <!-- Page Header -->
<?php echo $header;?>

  
<!-- Main content -->
<div class="container">
  <div class="row">
    <div class="col-6">
  <img src="./static/img/email.jpg" alt="" srcset="" class="img-fluid rounded">
    </div>
    <div class="col-6">
  <?php echo $formu_tony; ?>

</div>
  </div>
</div>

        <!-- Pager -->
        <div class="clearfix">
          <a class="btn btn-primary float-right" href="/ejercici5.php">IR a Cotización</a>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <?php echo $footer;?>

  <!-- Bootstrap core JavaScript -->
  <script src="./static/vendor/jquery/jquery.min.js"></script>
  <script src="./static/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="./static/js/clean-blog.min.js"></script>

</body>

</html>
