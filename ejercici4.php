<?php
libxml_use_internal_errors(TRUE);

//  E05 - Captura d'imatges externes

// A la pàgina https://www.portalmochis.net/humor/comic/ tenim un llistat d'imatges.

// Fes un script que ens les mostri totes les imatges en una taula centrada de tres columnes.

// Per fer-ho obtenir el codi HTML mitjançant la funció file_get_contents i fer servir funcions de cadena com strpos y substr extreure totes les imatges del llistat. També pots mirar la funció str_replace.


$files_pages = file_get_contents('https://www.portalmochis.net/humor/comic/');
$fin=false;
$array = Array();

// Cogemos todos los enlaces y los metemos en una array.
while(!$fin){
  $inicio = strpos($files_pages,'<a href=');
  $final = strpos($files_pages,'</a>');
  if($inicio !==false && $final !== false){
    array_push($array,substr($files_pages,$inicio,($final-$inicio)+4));
    $files_pages = substr($files_pages, $final+3, strlen($files_pages)+4);
  }else{
    $fin=true;
}
}


// procesamos la array de enlaces  y filtramos los que tengan imagenes en su interior.
$i=0;
$tres = Array();
$final_html='';

foreach ($array as $key => $value) {

  $inicio = strpos($value,'a href=');
  $jpg = strpos($value,'.jpg');
  $png = strpos($value,'.png');
  $gif = strpos($value,'.gif');

  if($jpg !==false){
    //gen_tablita(substr($value,$inicio+8,($jpg-$inicio)-4));
    $value=substr($value,$inicio+8,($jpg-$inicio)-4);
    $tres[$i]=$value;
  }elseif($png !==false){
    //gen_tablita(substr($value,$inicio+8,($png-$inicio)-4));
    $value= substr($value,$inicio+8,($png-$inicio)-4);
    $tres[$i]=$value;
  }elseif($gif !==false){
    //gen_tablita(substr($value,$inicio+8,($gif-$inicio)-4));
    $value = substr($value,$inicio+8,($gif-$inicio)-4);
    $tres[$i]=$value;
  }


  if(count($tres)-1==2){
    $final_html .= gen_tablita($tres);
    $tres = Array();
    $i=0;

  }else{
    $i++;
  }
}

// con esta función generamos las tablas con una array de 3 elementos.
function gen_tablita(array $url){
  $count= 0;
  $html='';
  foreach ($url as $key => $value) {
    
    if($count==0){
      $html .= '<tr>';
    }
    $html .= '<td><div class="card " style="width: 18rem;"><img class="card-img-top" src="'.'https://www.portalmochis.net'.$value.'" alt="Card image cap"></div></td>';
    if ($count==2) {
      $html .= '</tr>';
    }
    $count++;
  }
  return $html;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <title>E05 - Captura d'imatges externes</title>
</head>

<body>
   <div class="container">
    <div class="row text-align-center m-5">
      <h1 class="text-center">E05 - Captura d'imatges externes</h1>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">primer</th>
            <th scope="col">segon</th>
            <th scope="col">tercer</th>
          </tr>
        </thead>
        <tbody>
        <?php print_r($final_html);?>
        </tbody>
      </table>
    </div>
  </div>


  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
  </script>

</body>

</html>