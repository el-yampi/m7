<?php
// Tony dice que se comenta al final porque no se puede mostrar los errores en producción.
//error_reporting(E_ALL);
 
// Motrar todos los errores de PHP
//ini_set('error_reporting', E_ALL);
/* Defineix les següents funcions per treballar amb arrays:

unique(array $array, bool $keepKeys = false): Remove the duplicates from an array.
key($key, array $array, bool $returnValue = false): Check if key exists.
in($value, array $array, bool $returnKey = false): Check is value exists in the array.
first(array $array): Returns the first element in an array.
last(array $array): Returns the last element in an array.
firstKey(array $array): Returns the first key in an array.
lastKey(array $array): Returns the last key in an array.
Per fer aquesta pràctica et pot ser interesant revisar les funcions marcades en negreta.
*/
$array = array("python" => "el mejor lenguaje de programación.","java" => "Da menos errores de Python.","php"=>"es un lenguaje muy practico.","php2"=>"es un lenguaje muy practico.","javascript"=>"es un lenguaje muy necesario");

function unique(array $array,bool $keepKeys = false){
        $original_array = $array;
        if($keepKeys){
            $keys =array_keys($array);
            $array = array_unique($array);
            $borrados= array_flip(array_keys(array_diff_key($original_array, $array)));
            foreach($borrados as $key => $value){
                $borrados[$key] = NULL;
            };
            return array_merge($array,$borrados);
        }else{
            return array_unique($array);
        }
}

function llave($key, array $array,bool $returnValue = false){
        if($returnValue){
            if(array_key_exists($key,$array)){
                return $array[$key];
            }
    }
}


function in($value, array $array,bool $returnKey = false){
    
    if($returnKey){
        $key=array_search($value,$array);
        if($key != NULL){
            return $key;
        } else{
            return "no he encontrado ese valor";
        }
    }
}


function first(array $array){
    if (!empty($array)) {
            foreach($array as $key => $unused) {
                return array($key => $array[$key]);
            }
    }
    else{
        return NULL;
    }
}

function last(array $array){
    if(!empty($array)){
        return array(array_keys($array)[count($array)-1] => $array[array_keys($array)[count($array)-1]]);
    }
    else{
        return null;
        }
    }

print_r(last($array));

function firstKey($array){
    if (!function_exists('array_key_first')) {
        if(!empty($array)){
            foreach($array as $key => $unused) {
                return $key;
            }
            return NULL;
        }
    }else{
        return array_key_first($array);
    }
}

function lastKey($array){
    if(!function_exists('array_key_last')){
        if(!empty($array)){
            return array_keys($array)[count($array)-1];
        }else{
            return null;
        }
    }else{
        return array_key_last($array);
    }
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>