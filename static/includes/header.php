<?php



$header = "
<header class=\"masthead\" style=\"background-image: url('./static/img/home-bg.jpg')\">
<div class=\"overlay\"></div>
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-lg-8 col-md-10 mx-auto\">
      <div class=\"site-heading\">
        <h1>$h1_title</h1>
        <span class=\"subheading\">$h1_span</span>
      </div>
    </div>
  </div>
</div>
</header>
";


$navbar = 
"<nav class=\"navbar navbar-expand-lg navbar-light fixed-top\" id=\"mainNav\">
    <div class=\"container-fluid\">
    <div class=\"dropdown\">
  <button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    $drowndown
  </button>
  $droplink
</div>
      <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        Menu
        <i class=\"fas fa-bars\"></i>
      </button>
      <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
        <ul class=\"navbar-nav ml-auto\">
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"index.php\">$inicio</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"?action=ejercici1\">$ejercicio 1</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"?action=ejercici2\">$ejercicio 2</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"?action=ejercici3\">$ejercicio 3</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"?action=ejercici4\">$ejercicio 4</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"?action=ejercici5\">$ejercicio 5</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"?action=formulario\">$formulario </a>
          </li>
          <li class=\"nav-item\">";
          if(isset($_SESSION["username"])){
            $navbar .="<div class=\"dropdown\">
                      <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"droplogin\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        "." <img src=\"";
                        if(isset($_SESSION["imagen"])){
                          $navbar .= $_SESSION["imagen"];
                        }else{
                          $navbar .= "static\img\LOL_Logo.jpg";
                        }
                        
                      $navbar .="\" class=\"img-responsive\" width=\"25px\" height=\"25px\"> ".$_SESSION["username"]."
                      </a>
                      <div class=\"dropdown-menu\" aria-labelledby=\"droplogin\">
                        <a class=\"dropdown-item\" href=\"?action=delogin\">Cerrar sessión</a>
                      </div>
                    </div>";
          }else{
            $navbar .="<a class=\"nav-link\" href=\"?action=login\">login</a>";
          }
          
          $navbar .="</li>
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"?action=registro\">registro</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>";

  $header .= $navbar;



?>