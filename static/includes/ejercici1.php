<?php
$html = "<div class=\"col-12\" id=\"ejercici1\">
<h1>$ejercicio 1:</h1>
<br />
<h3><strong>$ejercicio_1_h1</strong></h3>
<div class=\"row\">
<br />
<form action=\"index.php\" method=\"post\">
<div class=\"form-group\">
    <label for=\"exampleInputEmail1\">$input_email</label>
    <input list=\"numbers\" class=\"form-control\" id=\"number\" name=\"number\" required>
    <datalist id=\"numbers\">
        <option value=\"8\">
        <option value=\"10\">
        <option value=\"12\">
        <option value=\"14\">
        <option value=\"16\">
        <option value=\"18\">
        <option value=\"20\">
    </datalist>
</div>

<div class=\"form-group form-check\">
    <input type=\"checkbox\" class=\"form-check-input\" id=\"capitalizar\" name=\"capitalizar\">
    <label class=\"form-check-label\" for=\"capitalizar\">$form_1</label>
</div>
<div class=\"form-group form-check\">
    <input type=\"checkbox\" class=\"form-check-input\" id=\"especial\" name=\"especial\">
    <label class=\"form-check-label\" for=\"especial\">$form_2</label>
</div>
<button type=\"submit\" class=\"btn btn-primary\">$submit</button>
</form>
<div class=\"jumbotron jumbotron-fluid ml-5\">
<div class=\"container\">
    <h1 class=\"display-4\">$password</h1>
    <p class=\"lead\">$help_text_form</p>
</div>
</div>
</div>
</div>";



?>