<?php
    $drowndown = "Aukeratu zure hizkuntza";
    $inicio = "Hasi";
    $ejercicio = "Praktikatu";
    $ejercicio_1_h1 = "Pasahitz sortzaile sendoa.";
    $input_email = "Aukeratu zure zenbakia";
    $form_1 = "Letra larriz";
    $form_2 = "Balio berezia";
    $submit = "Bidali";
    $help_text_form = "Pasahitzak seguruak izan behar dira ;)";
    $bolsa_title_text = "Tonyren poltsa";
    $h1_title= "Tonyren ariketekin egindako sarea";
    $h1_span = "M7 ariketak </br> 2020 fudges onenak.";
    $footer_text = "Tonyren webgunea";
    $goHome = "Joan hasteko";
    $formulario = "formulario";
?>