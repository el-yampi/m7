<?php
// Crea un formulari de registre que doni la possibilitat a una nou usuari de donar-se d'alta a la teva pàgina.

// La informació a emmagatzemar és:

//     usuari i contrasenya.
//     dades personals: dni, nom i cognoms, data de naixement i sexe.
//     Adreça, codi postal, població, provincia i telèfon (opcionals)
//     Imatge de l'usuari

// Un cop verificada i validada la informació, i només si tota és correcte, s'analitzarà el fitxer seleccionat, validant que el fitxer sigui una imatge de les següents característiques:

//     tipus: imatge (extensió i mime-type)
//     tamany màxim: 2Mb

// De moment no s'ha d'emmagatzemar aquesta informació enlloc.... la tractarem més endevant.

$html = "<form action=\"?action=ejercici5\" method=\"post\" role=\"form\">
<h1> Loginador:</h1>
<div class=\"form-group\">
  <label for=\"username\">Usuari</label>
  <input type=\"text\" class=\"form-control\" id=\"username\" name=\"username\">
</div>
<div class=\"form-group\">
  <label for=\"password\">Contraseña</label>
  <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\">
</div>
<button type=\"submit\" class=\"btn btn-primary\" id=\"exito\">Enviar</button>
</form>";
?>