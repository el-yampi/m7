<?php



function sanitizar(string $titulo){
    $titulo = filter_var ($titulo, FILTER_SANITIZE_STRING);
    $titulo = filter_var($titulo, FILTER_SANITIZE_SPECIAL_CHARS,
            array('flags' => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_ENCODE_HIGH));
    $titulo = htmlspecialchars($titulo, ENT_QUOTES, 'UTF-8');
    $titulo = preg_replace('/\s\s+/', ' ', $titulo);
    return $titulo;
}
function sanitizar_email(string $email){
    $email = sanitizar($email);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    return $email;
}

function is_valid_nom(string $titulo){
    $error = true;
    if (preg_match("/[0-9]+/", $titulo) | $titulo == NULL | strpos($titulo,"<") | strpos($titulo,">")) {
        $error = false;
    }
    return $error;
};

function is_valid_email($email){
    $error = true;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL) | $email == NULL) {
        $error = false;
    }
    return $error;
};
function is_valid_mensaje($mensaje){
    $error = true;
    if ($mensaje == NULL) {
        $error = false;
    }
    return $error;
};


function gen_formu($titulo, $correo, $mensaje,$post){
    $enviar_xml=false;
    $formu = "<form action=\"formulario.php\" method=\"post\" role=\"form\">
    <div class=\"form-group\">
    <label for=\"nombre\">Formulario de contacto</label>";
    if($post){
        
        if(is_valid_nom($titulo) & is_valid_email($correo) & is_valid_mensaje($mensaje)){
            $fin ="<p class=\"text-success\">El formulario se ha validado con éxito</p>";
            $enviar_xml = true;
        }else{
            $fin ="<p class=\"text-danger\">Error al enviar el formulario. Vuelve a intentarlo</p>";
        }

        if(is_valid_nom($titulo)){
            $titulo = sanitizar($titulo);
            $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\"></div>";
        }else{
            $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta no enviar números o  el campo vacío :/</p> </div>";
        };
        if(is_valid_email($correo)){
            $correo = sanitizar_email($correo);
            $formu .= "<div class=\"form-group\"><label for=\"email\">Email address</label><input type=\"text\" class=\"form-control\" name=\"email\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"email\">Email address</label><input type=\"text\" class=\"form-control\" name=\"email\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta Escribir bien el correo:/</p></div>";
        };
        if(is_valid_mensaje($mensaje)){
            $mensaje = sanitizar($mensaje);
            $formu .= "<div class=\"form-group\"><textarea class=\"form-control\" name=\"mensaje\" rows=\"3\" value=\"$mensaje\"></textarea></div>";
        }else{
            $formu .= "<div class=\"form-group\"><textarea class=\"form-control\" name=\"mensaje\" rows=\"3\"></textarea><p class=\"text-danger\">Este campo esta vacío</p></div>";
        };

        $formu .=$fin;
    }else{
        $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\" placeholder=\"Don Tony\"></div><div class=\"form-group\"><label for=\"coreo\">Correo electronico:</label><input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"tony@tony.cat\"></div><div class=\"form-group\"><label for=\"mensaje\">Escribe tu mensaje</label>
        <textarea class=\"form-control\" name=\"mensaje\" rows=\"3\" placeholder=\"Cosas de Tony\"></textarea>
      </div>";
      
    }
    $formu .="<button class=\"btn btn-primary\" type=\"submit\" name=\"enviar_seguro\">Envia</button></form>";
    if($enviar_xml){
        save_xml_tony($titulo, $correo, $mensaje);
    }
    return $formu;
};



?>