<?php

function save_xml_tony($nombre, $correo, $mensaje){
    $filepath = './static/xml/tony.xml';
    $date = Time();
    if (file_exists($filepath)) {
        $doc = new DomDocument('1.0', 'UTF-8');
        $doc->load($filepath);
        $root = $doc->firstChild;
       
        $headNode = $doc->createElement("formu");
        $root->appendChild($headNode);
    
        $titleNode = $doc->createElement("nombre");
        $title_text = $doc-> createTextNode($nombre);
        $titleNode->appendChild($title_text);
        $headNode->appendChild($titleNode);



        $emailNode = $doc->createElement("email");
        $email_text = $doc-> createTextNode($correo);
        $emailNode->appendChild($email_text);
        $headNode->appendChild($emailNode);


        $mensajeNode = $doc->createElement("mensaje");
        $mensaje_text = $doc-> createTextNode($mensaje);
        $mensajeNode->appendChild($mensaje_text);
        $headNode->appendChild($mensajeNode);


        $fechaNode = $doc->createElement("fecha");
        $fecha_text = $doc-> createTextNode(date("Y-m-d H:i:s",$date));
        $fechaNode->appendChild($fecha_text);
        $headNode->appendChild($fechaNode);

        $doc->save($filepath);

    }

}

?>