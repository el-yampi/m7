<?php

function gen_formu_registro($nombre, $correo, $contrasenya,$post){

    $formu = "<form action=\"?action=registro\" method=\"post\" role=\"form\">
    <div class=\"form-group\">
    <label for=\"nombre\">Formulario de Registro</label>";
    if($post){
        if(is_valid_nom($nombre) & is_valid_email($correo) & is_valid_contrasenya($contrasenya)){
            $fin ="<p class=\"text-success\">El formulario se ha validado con éxito</p>";
        }else{
            $fin ="<p class=\"text-danger\">Error al enviar el formulario. Vuelve a intentarlo</p>";
        }

        if(is_valid_nom($nombre)){
            $nombre = sanitizar($nombre);
            $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\"></div>";
        }else{
            $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta no enviar números o  el campo vacío :/</p> </div>";
        };
        if(is_valid_email($correo)){
            $correo = sanitizar_email($correo);
            $formu .= "<div class=\"form-group\"><label for=\"email\">Email address</label><input type=\"text\" class=\"form-control\" name=\"email\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"email\">Email address</label><input type=\"text\" class=\"form-control\" name=\"email\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta Escribir bien el correo:/</p></div>";
        };
        if(is_valid_mensaje($contrasenya)){
            $mensaje = sanitizar($contrasenya);
            $formu .= "<div class=\"form-group\"><label class=\"form-control\" name=\"contrasenya\" value=\"$contrasenya\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label class=\"form-control\" name=\"contrasenya\"><p class=\"text-danger\">Este campo esta vacío</p></div>";
        };

        $formu .=$fin;
    }else{
        $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\" placeholder=\"Don Tony\"></div><div class=\"form-group\"><label for=\"correo\">Correo electronico:</label><input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"tony@tony.cat\"></div><div class=\"form-group\"><label for=\"contraseña\">Escribe tu contraseña</label>
        <input type=\"password\" class=\"form-control\" name=\"contrasenya\">
      </div>";
      
    }
    $formu .="<button class=\"btn btn-primary\" type=\"submit\" name=\"enviar_seguro\">Envia</button></form>";
    return $formu;
};



?>