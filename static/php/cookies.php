<?php


if(isset($_COOKIE["lang"])){
    $lang = $_COOKIE["lang"];
    $data = "./static/php/traducciones/content_".$_COOKIE["lang"].".php";
}else{
    $data =  './static/php/traducciones/content_es.php';
    $lang = 'es';
}


if(isset($_GET["lang"])){
    $lang = $_GET["lang"];
    switch ($lang) {
        case 'es':
            $data =  './static/php/traducciones/content_es.php';
            setcookie("lang", "es", time()+3600); 
            break;
        case 'ca':
            $data = './static/php/traducciones/content_ca.php';
            setcookie("lang", "ca", time()+3600); 
            break;
        case 'de':
            $data = './static/php/traducciones/content_de.php';
            setcookie("lang", "de", time()+3600); 
            break;
        case 'en':
            $data = './static/php/traducciones/content_en.php';
            setcookie("lang", "en", time()+3600); 
            break;
        case 'eu':
            $data = './static/php/traducciones/content_eu.php';
            setcookie("lang", "eu", time()+3600);
            break;
        case 'fr':
            $data = './static/php/traducciones/content_fr.php';
            setcookie("lang", "fr", time()+3600); 
            break;
        default:
            # Se mantiene en "es", así me evito los dramas.
            $lang = 'es';
            $data =  './static/php/traducciones/content_es.php';
            break;
    }
}





$droplink = "<div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">";
if($lang =='es'){
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=es\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/553/553376.svg\" heigth=\"25px\"  width=\"25px\"></a>";

}else{
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=es\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/197/197593.svg\" heigth=\"25px\" , width=\"25px\"></a>";
}

if($lang == 'ca'){
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=ca\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/553/553376.svg\" heigth=\"25px\"  width=\"25px\"></a>";
    
}else{
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=ca\"><img src=\"https://cdn4.iconfinder.com/data/icons/world-flags-12/512/Untitled-2-03-512.png\"  heigth=\"25px\" , width=\"25px\"></a>";
}

if($lang == "fr"){
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=es\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/553/553376.svg\" heigth=\"25px\"  width=\"25px\"></a>";
    
}else{
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=fr\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/197/197560.svg\" heigth=\"25px\" , width=\"25px\"></a>";
}

if($lang == "de"){
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=es\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/553/553376.svg\" heigth=\"25px\"  width=\"25px\"></a>";
    
}else{
    $droplink .="<a class=\"dropdown-item\" href=\"?lang=de\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/197/197571.svg\" heigth=\"25px\" , width=\"25px\"></a>";
}

if($lang == "eu"){
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=es\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/553/553376.svg\" heigth=\"25px\"  width=\"25px\"></a>";
    
}else{
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=eu\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/197/197517.svg\" heigth=\"25px\" , width=\"25px\"></a>";
}

if($lang == "en"){
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=es\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/553/553376.svg\" heigth=\"25px\"  width=\"25px\"></a>";
    
}else{
    $droplink .= "<a class=\"dropdown-item\" href=\"?lang=en\"><img src=\"https://www.flaticon.es/svg/static/icons/svg/197/197374.svg\" heigth=\"25px\" , width=\"25px\"></a>";
}


$droplink .="</div>";



?>



