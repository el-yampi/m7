<?php
#Ejercici 1
$capitalizar = false;
$especial = false;
$number = 8;



function clear_data($string){
    if(strpos($string,' ') !=false){
        return substr($string,0,5);
    }else{
        return $string;
    }
};

function gen_cabecera(array $array){
    $html="";
    $i=0;
    $j=0;
    foreach ($array as $key => $value) {
        if($i==0){
            foreach($value as $llave => $valor){
                if($j==0){
                    $html .='<table class= "table table-striped"><thead class="bg-dark text-light"><tr><td>'.$llave.'</td>';
                }else{
                    $html .= '<td>'.$llave.'</td>';
                }
                if($j==count($value)){
                    $html .='</tr></thead>';
                }
                $j++;
            }
        }
        return $html;
    }

}

function gen_tbody(array $array){
    
    $html ='<tbody>';
    $i = 0;
    foreach ($array as $key => $value) {
        $html .='<tr>';
        foreach($value as $llave => $valor){
            if(isset($_SESSION['old_cotis']) and $llave == 'ultima_coti'){
                $ultima_coti_old= $_SESSION['old_cotis'][$i]['ultima_coti'];
                $ultima_coti_old = str_replace(',','.',$ultima_coti_old);
                $ultima_coti_actual = str_replace(',','.',$valor);

                if(doubleval($ultima_coti_actual)>doubleval($ultima_coti_old)){
                    $html .='<td class="bg-success">'.$ultima_coti_actual.'</td>';
                }elseif(doubleval($ultima_coti_actual)==doubleval($ultima_coti_old)){
                    $html .='<td class="bg-dark">'.$ultima_coti_actual.'</td>';
                }else{
                    $html .='<td class="bg-danger">'.$ultima_coti_actual.'</td>';
                }
            };
            if($llave==""){

            }
            if($llave=="variacio"){
                $valor = str_replace(',','.',$valor);
                if(doubleval($valor) >0){
                    $html .='<td class="text-success">'.$valor.'</td>';
                }else{
                    $html .='<td class="text-danger">'.$valor.'</td>';
                }

            }
            
            if($llave=="variacio_percent"){
                $valor = str_replace(',','.',$valor);
                if(doubleval($valor) >0){
                    $html .='<td class="text-success">'.$valor.'</td>';
                }else{
                    $html .='<td class="text-danger">'.$valor.'</td>';
                }
                
            }

            if($llave != "variacio" and $llave !="variacio_percent" and $llave!="ultima_coti"){
                $html .='<td>'.$valor.'</td>';
            }
           
        }
        $html .='</tr>';
        $i++;
        //echo html_entity_decode($html);
    }
    $html .='</tbody>';

    return $html;
};

function gen_array_coti($files_pages){
    $cabeceras = ['nom','ticker','mercat','ultima_coti','divisa','variacio','variacio_percent','volum','mínim','màxim','data','hora'];
    $array = array();
    $fin = false;
    $datos_finales = array();
    $id=0;
    while(!$fin){
        $inicio = strpos($files_pages,'<tr id="tr_'.$id.'"');
        $final = strpos($files_pages,'</tr>',$inicio);
        if($inicio !==false && $final !== false){

            array_push($array,substr($files_pages,$inicio,($final-$inicio)+5));
            $files_pages = substr($files_pages, $final+3, strlen($files_pages)+5);
            $id++;
        }else{
            $fin=true;
        }
    }

    for($i=0;$i<count($array);$i++){
        $fin=false;
        $valor = $array[$i];
        $c = array();
        for($j=0;$j<count($cabeceras);$j++){
            $inicio = strpos($valor,'<td');
            $final = strpos($valor,'</td>',$inicio);
            if($j==3){
                $valor = substr($valor, $final+3, strlen($valor));
                $inicio = strpos($valor,'<td');
                $final = strpos($valor,'</td>',$inicio);
            }
            if($inicio !==false && $final !== false){
                $dato=trim(strip_tags(substr($valor,$inicio,($final-$inicio)+5)));
                $dato = clear_data($dato);
                $c[$cabeceras[$j]]=$dato;
                $valor = substr($valor, $final+3, strlen($valor));

            };
        };
    array_push($datos_finales,$c);
    };
        return $datos_finales;
};




contrasenya_random($number,$capitalizar,$especial);



function contrasenya_random($len = 8, $catitalize= false, $especial_char = false){
    global $password;
    
    // Restamos dos carateres para el número final.
    $length = $len - 2;
    // Revisamos que los parametros que recibimos cumplan con lo que necesitamos
    if($len >=6 && ($length %2)!= 0){
        $len = 8;
    }

    $conso = array('b','c','d','f','g','h','j','k','l','m','n','p','r','s','t','v','w','x','y','z');
    
    $vocal = array('a','e','i','o','u');
    
    $spchars = array('!','@','#','$','%','^','*','&','*','-','+','?');
    
    $password = '';

    for($i=1;$i<=($length/2);$i++){
      $password .=$conso[rand(0,19)];
      $password .= $vocal[rand(0,4)];
    }

    if ($catitalize == true){
      $password = ucfirst($password);
    }
    if($especial_char == true){
      $password = substr($password,0,-1).$spchars[rand(0,11)];
    }
    $password .= rand(10,99);
    return $password;
}

function gen_html_datalist($array_value=[],$send,$active='nulo'){
    $html ="";
    $html .='<label for="coches">Selecciona tu coche</label><br><select id="coches" name="valu">';
    $checkboxs='';
    if($send == false){
        foreach($array_value as $key => $value["checkbox"]){
            $html .='<option value="'.$key.'" >'.$key.'</option>';
        }
    }else{
            foreach($array_value as $key => $value["checkbox"]){
                if($key == $active){
                    $html .='<option selected value="'.$key.'">'.$key.'</option>';
                    foreach($value as $val => $j){
                        foreach($j["checkbox"] as $i){
                            $checkboxs .='<input type="checkbox" name="'.$i.
                            '" value="'.$i.'"><label for="'.$i.'">'.$i.'</label><br>';
                        }
                   }
                }else{
                    $html .='<option disabled value = "'.$key.'" >'.$key.'</option>';
                }
        }
        $html .= '</select>';
    }
    if($active != 'nulo'){
        $html .= '<br>'.$checkboxs;
    }
    return $html;

}

function gen_tablita(array $url){
    $count= 0;
    $html='';
    foreach ($url as $key => $value) {
      
      if($count==0){
        $html .= '<tr>';
      }
      $html .= '<td><div class="card " style="width: 18rem;"><img class="card-img-top" src="'.'https://www.portalmochis.net'.$value.'" alt="Card image cap"></div></td>';
      if ($count==2) {
        $html .= '</tr>';
      }
      $count++;
    }
    return $html;
  }
  function gen_user_checkbox(){
    $text='<p>Valores seleccionados:</p><ul>';
    foreach($_POST as $nombre_campo => $valor){
        if($nombre_campo != 'valu'){
            $text .='<li>'.$nombre_campo.'</li>';
        }
    }
    return $text;
}

function unique(array $array,bool $keepKeys = false){
    $original_array = $array;
    if($keepKeys){
        $keys =array_keys($array);
        $array = array_unique($array);
        $borrados= array_flip(array_keys(array_diff_key($original_array, $array)));
        foreach($borrados as $key => $value){
            $borrados[$key] = NULL;
        };
        return array_merge($array,$borrados);
    }else{
        return array_unique($array);
    }
}

function llave($key, array $array,bool $returnValue = false){
    if($returnValue){
        if(array_key_exists($key,$array)){
            return $array[$key];
        }
}
}


function in($value, array $array,bool $returnKey = false){

if($returnKey){
    $key=array_search($value,$array);
    if($key != NULL){
        return $key;
    } else{
        return "no he encontrado ese valor";
    }
}
}


function first(array $array){
if (!empty($array)) {
        foreach($array as $key => $unused) {
            return array($key => $array[$key]);
        }
}
else{
    return NULL;
}
}

function last(array $array){
if(!empty($array)){
    return array(array_keys($array)[count($array)-1] => $array[array_keys($array)[count($array)-1]]);
}
else{
    return null;
    }
}


function firstKey($array){
if (!function_exists('array_key_first')) {
    if(!empty($array)){
        foreach($array as $key => $unused) {
            return $key;
        }
        return NULL;
    }
}else{
    return array_key_first($array);
}
}

function lastKey($array){
if(!function_exists('array_key_last')){
    if(!empty($array)){
        return array_keys($array)[count($array)-1];
    }else{
        return null;
    }
}else{
    return array_key_last($array);
}
}

function sanitizar(string $titulo){
    $titulo = filter_var ($titulo, FILTER_SANITIZE_STRING);
    $titulo = filter_var($titulo, FILTER_SANITIZE_SPECIAL_CHARS,
            array('flags' => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_ENCODE_HIGH));
    $titulo = htmlspecialchars($titulo, ENT_QUOTES, 'UTF-8');
    $titulo = preg_replace('/\s\s+/', ' ', $titulo);
    return $titulo;
}
function sanitizar_email(string $email){
    $email = sanitizar($email);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    return $email;
}

function is_valid_nom(string $titulo){
    $error = true;
    if (preg_match("/[0-9]+/", $titulo) | $titulo == NULL | strpos($titulo,"<") | strpos($titulo,">")) {
        $error = false;
    }
    return $error;
};

function is_valid_email($email){
    $error = true;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL) | $email == NULL) {
        $error = false;
    }
    return $error;
};
function is_valid_mensaje($mensaje){
    $error = true;
    if ($mensaje == NULL) {
        $error = false;
    }
    return $error;
};

function is_valid_imagen(){
    $error = false;
    $extensiones = array(0=>'image/jpg',1=>'image/jpeg',2=>'image/png');
    $max_tamanyo =((1024 * 1024)*2)* 8;
    if(isset($_FILES['imagen'])){
        if (in_array($_FILES['imagen']['type'], $extensiones) & $_FILES['imagen']['type'] < $max_tamanyo) {
            $error = true;
            $name = basename($_FILES["imagen"]["name"]);
            move_uploaded_file($_FILES['imagen']['tmp_name'], "imagen/".$name);
            $_SESSION["imagen"]="imagen/$name";
        }
    }

     return $error;
}
function getOS() { 

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	$os_platform =   "Bilinmeyen İşletim Sistemi";
	$os_array =   array(
		'/windows nt 10/i'      =>  'Windows 10',
		'/windows nt 6.3/i'     =>  'Windows 8.1',
		'/windows nt 6.2/i'     =>  'Windows 8',
		'/windows nt 6.1/i'     =>  'Windows 7',
		'/windows nt 6.0/i'     =>  'Windows Vista',
		'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
		'/windows nt 5.1/i'     =>  'Windows XP',
		'/windows xp/i'         =>  'Windows XP',
		'/windows nt 5.0/i'     =>  'Windows 2000',
		'/windows me/i'         =>  'Windows ME',
		'/win98/i'              =>  'Windows 98',
		'/win95/i'              =>  'Windows 95',
		'/win16/i'              =>  'Windows 3.11',
		'/macintosh|mac os x/i' =>  'Mac OS X',
		'/mac_powerpc/i'        =>  'Mac OS 9',
		'/linux/i'              =>  'Linux',
		'/ubuntu/i'             =>  'Ubuntu',
		'/iphone/i'             =>  'iPhone',
		'/ipod/i'               =>  'iPod',
		'/ipad/i'               =>  'iPad',
		'/android/i'            =>  'Android',
		'/blackberry/i'         =>  'BlackBerry',
		'/webos/i'              =>  'Mobile'
	);

	foreach ( $os_array as $regex => $value ) { 
		if ( preg_match($regex, $user_agent ) ) {
			$os_platform = $value;
		}
	}   
	return $os_platform;
}

function getBrowser() {
	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	$browser        = "Bilinmeyen Tarayıcı";
	$browser_array  = array(
		'/msie/i'       =>  'Internet Explorer',
		'/firefox/i'    =>  'Firefox',
		'/safari/i'     =>  'Safari',
		'/chrome/i'     =>  'Chrome',
		'/edge/i'       =>  'Edge',
		'/opera/i'      =>  'Opera',
		'/netscape/i'   =>  'Netscape',
		'/maxthon/i'    =>  'Maxthon',
		'/konqueror/i'  =>  'Konqueror',
		'/mobile/i'     =>  'Handheld Browser'
	);

	foreach ( $browser_array as $regex => $value ) { 
		if ( preg_match( $regex, $user_agent ) ) {
			$browser = $value;
		}
	}
	return $browser;
}

function is_valid_data($data){
    if (preg_match("/^(((0[1-9]|[12]\d|3[01])\-(0[13578]|1[02])\-((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\-(0[13456789]|1[012])\-((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\-02\-((19|[2-9]\d)\d{2}))|(29\-02\-((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/",$data)) {
        return true;
    } else {
        return false;
    }
}


function is_valid_dni($dni){
    $letra = substr($dni, -1);
    $numeros = substr($dni, 0, -1);
    $valido;
    if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
      $valido=true;
    }else{
      $valido=false;
    }
    return $valido;
}
function gen_formu($titulo, $correo, $mensaje,$post){
    $enviar_xml=false;
    $formu = "<form action=\"index.php?action=formulario\" method=\"post\" role=\"form\">
    <div class=\"form-group\">
    <label for=\"nombre\">Formulario de contacto</label>";
    if($post){
        
        if(is_valid_nom($titulo) & is_valid_email($correo) & is_valid_mensaje($mensaje)){
            $fin ="<p class=\"text-success\">El formulario se ha validado con éxito</p>";
            $enviar_xml = true;
        }else{
            $fin ="<p class=\"text-danger\">Error al enviar el formulario. Vuelve a intentarlo</p>";
        }

        if(is_valid_nom($titulo)){
            $titulo = sanitizar($titulo);
            $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\"></div>";
        }else{
            $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta no enviar números o  el campo vacío :/</p> </div>";
        };
        if(is_valid_email($correo)){
            $correo = sanitizar_email($correo);
            $formu .= "<div class=\"form-group\"><label for=\"email\">Email address</label><input type=\"text\" class=\"form-control\" name=\"email\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"email\">Email address</label><input type=\"text\" class=\"form-control\" name=\"email\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta Escribir bien el correo:/</p></div>";
        };
        if(is_valid_mensaje($mensaje)){
            $mensaje = sanitizar($mensaje);
            $formu .= "<div class=\"form-group\"><textarea class=\"form-control\" name=\"mensaje\" rows=\"3\" value=\"$mensaje\"></textarea></div>";
        }else{
            $formu .= "<div class=\"form-group\"><textarea class=\"form-control\" name=\"mensaje\" rows=\"3\"></textarea><p class=\"text-danger\">Este campo esta vacío</p></div>";
        };

        $formu .=$fin;
    }else{
        $formu .= "<input type=\"text\" class=\"form-control\" name=\"nombre\" placeholder=\"Don Tony\"></div><div class=\"form-group\"><label for=\"coreo\">Correo electronico:</label><input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"tony@tony.cat\"></div><div class=\"form-group\"><label for=\"mensaje\">Escribe tu mensaje</label>
        <textarea class=\"form-control\" name=\"mensaje\" rows=\"3\" placeholder=\"Cosas de Tony\"></textarea>
      </div>";
      
    }
    $formu .="<button class=\"btn btn-primary\" type=\"submit\" name=\"enviar_seguro\">Envia</button></form>";
    if($enviar_xml){
        save_xml_tony($titulo, $correo, $mensaje);
    }
    return $formu;
};


function check_email_exists_db($correo){
    $r = false;
    include 'mysql.php';
    if ($enlace_consulta) {
        $sql = "SELECT * FROM tbl_usuaris WHERE tbl_usuaris.email='$correo';";
        if (!$resultado = $enlace_consulta->query($sql)) {
            mysqli_close($enlace_consulta);
         }else{
            $tony_data = $resultado->fetch_assoc();
            if($tony_data['email'] !=null){
                $r =true;
            };
            mysqli_close($enlace_consulta);
         }
    }else{
        mysqli_close($enlace_consulta);
    }
    return $r;
}
function insert_user($correo,$password,$tipusident,$nombre,$apellido,$sexe,$data,$adreca,$postal,$poblacion,$provincia,$imagen,$navegador,$plataforma,$dataCreacio,$dataDarrerAcces,$telefon,$dni){
    include 'users.php';
    $r=false;
    if($sexe == "hombre"){
        $sexe = "home";
    }else{
        $sexe == "dona";
    }
    if(count($postal) < 4){
        $postal = null;
    }
    if (!$enlace_generic) {
        die("Connection failed: " . mysqli_connect_error());
      }
    $sql = "INSERT INTO tbl_usuaris (email,password,tipusIdent,numeroIdent,nom,cognoms,sexe,datanaixement,adreca,codiPostal,poblacio,provincia,telefon,imatge,status,navegador,plataforma,dataCreacio,dataDarrerAcces) VALUES ('$correo','$password','1','$dni','$nombre','$apellido','$sexe','$data','$adreca',null,'$poblacion','$provincia',null,'$imagen',0,'$navegador','$plataforma','$dataCreacio','$dataDarrerAcces')";
    if (mysqli_query($enlace_generic,$sql)) {
        $r=true;
     }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($enlace_generic);
      }
     mysqli_close($enlace_generic);
     return $r;
}
function return_id(){
    $r = 0;
    include 'mysql.php';
    if ($enlace_consulta) {
        $sql = "SELECT count(*) FROM tbl_usuaris;";
        if (!$resultado = $enlace_consulta->query($sql)) {
            mysqli_close($enlace_consulta);
         }else{
            $tony_data = $resultado->fetch_assoc();
            if($tony_data['count(*)'] !=null){
                $r =$tony_data["count(*)"];
            };
            mysqli_close($enlace_consulta);
         }
    }else{
        mysqli_close($enlace_consulta);
    }
    return $r;
}

function revisar_usuario($id){
    $r = false;
    include 'mysql.php';
    if ($enlace_consulta) {
        $sql = "SELECT count(*) FROM tbl_usuaris where id=$id;";
        if (!$resultado = $enlace_consulta->query($sql)) {
            mysqli_close($enlace_consulta);
         }else{
            $tony_data = $resultado->fetch_assoc();
            if($tony_data['count(*)'] !=null){
                $r =$tony_data["count(*)"];
            };
            mysqli_close($enlace_consulta);
         }
    }else{
        mysqli_close($enlace_consulta);
    }
    return $r;
}

function activar($id){
    include 'users.php';
    $r=false;
    $sql = "UPDATE tbl_usuaris SET status=1 where id=$id;";
    if ($resultado = mysqli_query($enlace_generic,$sql)) {
        $r=true;
     }
     mysqli_close($enlace_generic);
     return $r;
}
function username_id($username){
    include 'mysql.php';
    $r =0;
        $sql = "SELECT id from tbl_usuaris where email='$username';";
        if ($resultado = mysqli_query($enlace_consulta,$sql)) {
            $row = mysqli_fetch_array($resultado);
            $r = $row["id"];
            
         }else{
            echo "Error: " . $sql . "<br>" . mysqli_error($enlace_consulta);

         }
         mysqli_close($enlace_consulta);
    return $r;
}
function login_usuario($email,$password){
    include 'mysql.php';
    $r =3;
    if(is_valid_email($email) & is_valid_mensaje($password)){
        $sql = "SELECT * from tbl_usuaris where email='$email';";
        if ($resultado = mysqli_query($enlace_consulta,$sql)) {
            $row = mysqli_fetch_array($resultado);
            if($row["password"]==$password & $row["status"] == 1){
                $r=1;
            };
            if($row["password"]==$password & $row["status"] == 2){
                $r=2;
            }
            if($row["password"]==$password & $row["status"] == 0){
                $r=0;
            }
            
         }else{
            echo "Error: " . $sql . "<br>" . mysqli_error($enlace_consulta);

         }
         mysqli_close($enlace_consulta);
    }else{
        echo "ERROR";
    }
    return $r;
}

function get_db_name($email){
    include 'mysql.php';
    $r =0;
    if(is_valid_email($email)){
        $sql = "SELECT * from tbl_usuaris where email='$email';";
        if ($resultado = mysqli_query($enlace_consulta,$sql)) {
            $row = mysqli_fetch_array($resultado);
            $r = $row["nom"];
            
         }else{
            echo "Error: " . $sql . "<br>" . mysqli_error($enlace_consulta);

         }
         mysqli_close($enlace_consulta);
    }else{
        echo "ERROR";
    }
    return $r;
}
function id_user($email){
    include 'mysql.php';
    $r=0;
    $sql = "SELECT id FROM tbl_usuaris WHERE email='$email';";

     if (!$enlace_consulta) {
        die("Connection failed: " . mysqli_connect_error());
      }
     if ($result=mysqli_query($enlace_consulta,$sql)) {
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
              $r=$row["id"];
            }
     }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($enlace_consulta);
      }
     mysqli_close($enlace_consulta);
     return $r;
}
}

function get_db_imagen($email){
    include 'mysql.php';
    $r='';
    $sql = "SELECT imatge FROM tbl_usuaris WHERE email='$email';";

     if (!$enlace_consulta) {
        die("Connection failed: " . mysqli_connect_error());
      }
     if ($result=mysqli_query($enlace_consulta,$sql)) {
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
              $r=$row["imatge"];
            }
     }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($enlace_consulta);
      }
     mysqli_close($enlace_consulta);
     return $r;
}
}

function actualizar_db_acceso($username){
    include 'users.php';
    $today = date("Y-m-d H:i:s");
    $sql = "UPDATE tbl_usuaris SET dataDarrerAcces='$today' where email='$username'";
    if ($resultado = mysqli_query($enlace_generic,$sql)) {
        $r=true;
     }else{
        echo "Error: " . $sql . "<br>" . mysqli_error($enlace_generic); 
     }
     mysqli_close($enlace_generic);
}



function gen_formu_registro($nombre,$apellido, $correo, $contrasenya,$dni,$data,$sexe,$adreca,$postal,$poblacion,$provincia,$telefon,$imagen,$post){
    $terminar = false;
    $formu = "<form action=\"?action=registro\" method=\"post\" role=\"form\" enctype=\"multipart/form-data\">
    <p>Formulario de Registro:</p>";
    if($post){
        if(is_valid_nom($nombre) & is_valid_nom($apellido) & is_valid_email($correo) & is_valid_mensaje($contrasenya) & is_valid_dni($dni) & is_valid_mensaje($contrasenya) & !check_email_exists_db($correo)){
            $fin ="<p class=\"text-success\">El formulario se ha validado con éxito</p>";
            $terminar = true;

        }else{
            $fin ="<p class=\"text-danger\">Error al enviar el formulario. Vuelve a intentarlo</p>";
        }

        if(is_valid_nom($nombre)){
            $nombre = sanitizar($nombre);
            $formu .= "<div class=\"form-group\"><label for=\"nombre\">nombre</label><input type=\"text\" class=\"form-control\" name=\"nombre\" value=\"$nombre\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"nombre\">nombre</label><input type=\"text\" class=\"form-control\" name=\"nombre\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta no enviar números o  el campo vacío :/</p> </div>";
        };
        if(is_valid_nom($apellido)){
            $apellido = sanitizar($apellido);
            $formu .= "<div class=\"form-group\"><label for=\"apellido\">apellido</label><input type=\"text\" class=\"form-control\" name=\"apellido\" value=\"$apellido\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"apellido\">apellido</label><input type=\"text\" class=\"form-control\" name=\"apellido\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta no enviar números o  el campo vacío :/</p> </div>";
        };
        if(is_valid_email($correo) & !check_email_exists_db($correo)){
            $correo = sanitizar_email($correo);
            $formu .= "<div class=\"form-group\"><label for=\"email\">Email address</label><input type=\"text\" class=\"form-control\" name=\"email\" value=\"$correo\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"email\">Email address</label><input type=\"text\" class=\"form-control\" name=\"email\"><p class=\"text-danger\">Este campo no se ha podido procesar por las condiciones impuestas por Tony, intenta Escribir bien el correo:/</p></div>";
        };
        if($contrasenya != null){
            $formu .= "<div class=\"form-group\"><label for=\"contrasenya\">Contrasenya</label><input type=\"password\" class=\"form-control\" name=\"contrasenya\" value=\"$contrasenya\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"contrasenya\">Contrasenya</label><input type=\"password\" class=\"form-control\" name=\"contrasenya\"><p class=\"text-danger\">Este campo esta vacío</p></div>";
        };
        if(is_valid_dni($dni)){
            $formu .= "<div class=\"form-group\"><label for=\"dni\">DNI</label><input type=\"dni\" class=\"form-control\" name=\"dni\" value=\"$dni\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"dni\">DNI</label><input type=\"dni\" class=\"form-control\" name=\"dni\"><p class=\"text-danger\">Este campo esta incorrecto</p></div>";
        }
        if(is_valid_data($data)){
            $formu .= "<div class=\"form-group\"><label for=\"data\">Fecha</label><input type=\"data\" class=\"form-control\" name=\"data\" value=\"$data\"></div>";
        }else{
            $formu .= "<div class=\"form-group\"><label for=\"data\">Fecha</label><input type=\"data\" class=\"form-control\" name=\"data\"><p class=\"text-warning\">Este campo no se ha podido procesar correctamente</p></div>";
        }
        if($sexe == "hombre" | $sexe == "mujer"){
            $formu .= "<div class=\"form-check\">
            <input class=\"form-check-input\" type=\"radio\" name=\"genero\" id=\"genero1\" value=\"$sexe\" checked>
            <label class=\"form-check-label\" for=\"genero\">
              $sexe
            </label>
          </div>";
        }else{
            $formu .= "<div class=\"form-check\">
            <input class=\"form-check-input\" type=\"radio\" name=\"genero\" id=\"genero1\" value=\"sin\" checked>
            <label class=\"form-check-label\" for=\"genero\">
              No definido
            </label>
          </div>";
        }
        if (is_valid_mensaje($adreca)){
            $adreca = sanitizar($adreca);
            $formu .="<div class=\"form-row\">
            <div class=\"col-12 my-2\">
            <input type=\"text\" class=\"form-control bg-success\" name=\"adreca\" value=\"$adreca\">
            </div></div>";
        }else{
            $formu .="<div class=\"form-row\">
            <div class=\"col-12\ my-2\">
            <input type=\"text\" class=\"form-control bg-warning\" name=\"provincia\" placeholder=\"provincia\">
            </div></div>";
        }
        if (is_valid_mensaje($provincia)){
            $provincia = sanitizar($provincia);
            $formu .="<div class=\"form-row\">
            <div class=\"col-6\">
            <input type=\"text\" class=\"form-control bg-success\" name=\"provincia\" value=\"$provincia\">
            </div>";
        }else{
            $formu .="<div class=\"form-row\">
            <div class=\"col-6\">
            <input type=\"text\" class=\"form-control bg-warning\" name=\"provincia\" placeholder=\"provincia\">
            </div>";
        }
        
        if (is_valid_mensaje($poblacion)){
            $poblacion = sanitizar($poblacion);
            $formu .="<div class=\"col-3\">
            <input type=\"text\" class=\"form-control bg-success\" name=\"poblacion\" value=\"$poblacion\">
            </div>";
        }else{
            $formu .="<div class=\"col-3\">
            <input type=\"text\" class=\"form-control bg-warning\" name=\"poblacion\" placeholder=\"poblacion\">
            </div>";
        }

        if (is_valid_mensaje($postal)){
            $postal = sanitizar($postal);
            $formu .="<div class=\"col-3\">
            <input type=\"text\" class=\"form-control bg-success\" name=\"postal\" value=\"$postal\">
            </div></div>";
        }else{
            $formu .="<div class=\"col-3\">
            <input type=\"text\" class=\"form-control bg-warning\" name=\"postal\" placeholder=\"postal\">
            </div></div>";
        }

        if (is_valid_imagen()){
            $formu .="<div class=\"form-group\">
            <label class=\"text-success\" for=\"imagen\">Tu imagen</label>
            <input type=\"file\" class=\"form-control-file\" id=\"imagen\">
            </div>";
        }else{
            $formu .="<div class=\"form-group\">
            <label class=\"text-warning\" for=\"imagen\">Tu imagen</label>
            <input type=\"file\" class=\"form-control-file\" id=\"imagen\">
            <p class=\"text-warning\">Se ha de subir imagen buena</p>
            </div>";
        }
        $formu .=$fin;
    }else{
        $formu .= "<div class=\"form-group\">
        <label for=\"nombre\">Escribe tu nombre</label><input type=\"text\" class=\"form-control\" name=\"nombre\" placeholder=\"Don Tony\"></div><div class=\"form-group\">
        <label for=\"apellido\">Escribe tu Apellido</label>
                <input type=\"text\" class=\"form-control\" name=\"apellido\" placeholder=\"Agilar\"><div class=\"form-group\">
        </div><div class=\"form-group\"><label for=\"correo\">Correo electronico:</label><input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"tony@tony.cat\"></div><div class=\"form-group\"><label for=\"contraseña\">Escribe tu contraseña</label>
        <input type=\"password\" class=\"form-control\" name=\"contrasenya\"></div><div class=\"form-group\">
        <label for=\"dni\">Escribe tu DNI</label>
                <input type=\"text\" class=\"form-control\" name=\"dni\"><div class=\"form-group\">
        </div>
        <div class=\"form-group\">
<label for=\"data\">Fecha de nacimiento</label>
        <input type=\"text\" class=\"form-control\" name=\"data\" placeholder=\"dd-mm-yyyy\"><div class=\"form-group\">
</div>
<div class=\"form-check\">
  <input class=\"form-check-input\" type=\"radio\" name=\"genero\" id=\"genero1\" value=\"hombre\" checked>
  <label class=\"form-check-label\" for=\"genero\">
    hombre
  </label>
</div>
<div class=\"form-check\">
  <input class=\"form-check-input\" type=\"radio\" name=\"genero\" id=\"genero2\" value=\"mujer\">
  <label class=\"form-check-label\" for=\"genero\">
    mujer
  </label>
</div>
<div class=\"form-row\">
<div class=\"col-12 my-2\">
  <input type=\"text\" class=\"form-control\" name=\"adreca\" placeholder=\"direccion\">
</div>
<div class=\"col-7\">
  <input type=\"text\" class=\"form-control\" name=\"provincia\" placeholder=\"provincia\">
</div>
<div class=\"col\">
  <input type=\"text\" class=\"form-control\" name=\"poblacion\" placeholder=\"poblacion\">
</div>
<div class=\"col\">
  <input type=\"text\" class=\"form-control\" name=\"postal\" placeholder=\"postal\">
</div>
</div>
<div class=\"form-group\">
<label for=\"imagen\">Tu imagen</label>
<input type=\"file\" class=\"form-control-file\" id=\"imagen\" name=\"imagen\">
</div>
";
      
    }

    if($terminar){
        //Guardamos los datos una vez validado y salimos
        $id = return_id()+1;
        $imagen = ".\imagen/generic.png";
        if(isset($_SESSION["imagen"])){
            $imagen = $_SESSION["imagen"];
        }
        $formu .="<button class=\"btn btn-primary mt-5\" type=\"submit\" name=\"enviar_seguro\">Volver a enviar</button></form>";
        $formu .="<a class=\"btn border\" href=\"?action=final_feliz&email=$correo&password=$contrasenya&tipusident=1&nom=$nombre&cognom=$apellido&sexe=$sexe&datanaixement=$data&adreca=$adreca&id=$id&imagen=$imagen&postal=$postal&poblacion=$poblacion&provincia=$provincia&dni=$dni&telefon=$telefon\">Registarse y salir</a>";
    }else{
        $formu .="<button class=\"btn btn-primary mt-5\" type=\"submit\" name=\"enviar_seguro\">Envia</button></form>";
        $formu .="<a class=\"btn border mt-5\" href=\"?action=final_triste\">Salir</a>";
    }
    
    return $formu;
};




?>
